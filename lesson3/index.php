<html>
<head>
    <title>Lesson 3</title>
    <meta charset="utf-8">
</head>
<body>
    <?php
        ini_set("error_reporting", E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
    ?>
    <h1>Задание 1</h1>
    <p>
        С помощью цикла while вывести все числа в промежутке от 0 до 100, которые делятся на 3 без остатка.
    </p>
    <?php
        $i = 0;
        while ($i <= 100) {
            if ($i % 3 == 0) {
                echo $i . " ";
            }
            $i++;
        }
    ?>
    <h1>Задание 2</h1>
    <p>
        С помощью цикла do...while написать функцию для вывода чисел от 0 до 10, чтобы результат выглядел так:
        <br>0 – это ноль.
        <br>1 – нечётное число.
        <br>2 – чётное число.
        <br>3 – нечётное число.
        <br>...
        <br>10 – чётное число.
    </p>
    <?php
        $i = 0;
        do {
            if ($i == 0) {
                echo $i ." - это ноль<br>";
            } elseif ($i % 2 == 0) {
                echo $i . " - четное число<br>";
            } else {
                echo $i . " - нечетное число<br>";
            }
            $i++;
        } while ($i<=10)
    ?>
    <h1>Задание 3</h1>
    <p>
        Объявить массив, в котором в качестве ключей будут использоваться названия областей, а в качестве значений – массивы с названиями городов из соответствующей области.
        <br>Вывести в цикле значения массива, чтобы результат был таким:
        <br>Московская область:
        <br>Москва, Зеленоград, Клин.
        <br>Ленинградская область:
        <br>Санкт-Петербург, Всеволожск, Павловск, Кронштадт.
        <br>Рязанская область...(названия городов можно найти на maps.yandex.ru)
        <br>-------------
    </p>
    <?php
        $cities = [
            'Московская область' => [
                'Москва',
                'Зеленоград',
                'Клин',
                'Красногорск',
                'Королев'
            ],
            'Ленинградская область' => [
                'Санкт-Перербург',
                'Всеволожск',
                'Кудрово',
                'Павловск',
                'Кронштадт'
            ],
            'Владимирская область'=> [
                'Владимир',
                'Александров',
                'Гусь-Хрустальный',
                'Петушки',
                'Ковров',
            ],
            'Томская область' => [
                'Омск',
                'Томск',
                'Кожевниково',
                'Асино',
                'Комсомольск'
            ]
        ];
        foreach ($cities as $value => $city) {
            echo "<p>" . $value . ":</p>";
            foreach ($city as $value) {
                echo $value . ", ";
            }
        }
    ?>
    <h1>Задание 4</h1>
    <p>
        Объявить массив, индексами которого являются буквы русского языка, а значениями – соответствующие латинские буквосочетания (‘а’=> ’a’, ‘б’ => ‘b’, ‘в’ => ‘v’, ‘г’ => ‘g’, ..., ‘э’ => ‘e’, ‘ю’ => ‘yu’, ‘я’ => ‘ya’).
        <br>Написать функцию транслитерации строк.
    </p>
    <form method="POST">
        <p>Строка: <input name="task4_str" type="text"></p>
        <p><button>Транслит</button></p>
    <?php
        $alphabet = [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'yo',
            'ж' => 'j',
            'з' => 'z',
            'и' => 'i',
            'й' => 'yi',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sh',
            'ъ' => "'",
            'ы' => 'y',
            'ь' => "'",
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            ' ' => '_'
        ];

        function translit($str, $alphabet)
        {
            for ($i = 0; $i <= strlen($str); $i++) {
                $a = mb_substr($str, $i, 1);
                if (array_key_exists($a, $alphabet)) {
                    echo $alphabet[$a];
                } else {
                    echo $a;
                }
                
            }
        }

        if ($_POST && $_POST['task4_str']) {
            $str = $_POST['task4_str'];
            translit($str, $alphabet);
        } else {
            echo 'POST пуст';
        }
    ?>
    <h1>Задание 5</h1>
    <p>
        Написать функцию, которая заменяет в строке пробелы на подчеркивания и возвращает видоизмененную строчку.
    </p>
    <form method="POST">
        <p>Строка: <input name="task5_str" type="text"></p>
        <p><button>Отправить</button></p>
    <?php
        function spaceToUnderline($str) 
        {
            for($i = 0; $i <= strlen($str); $i++) {
                if (mb_substr($str, $i, 1) == " ") {
                    $str[$i] = "_";
                } 
            }
            return $str;
        }

        if ($_POST && $_POST['task5_str']) {
            $str = $_POST['task5_str'];
            echo spaceToUnderline($str);
        } else {
            echo "POST пуст";
        }
    ?>
    <h1>Задание 7</h1>
    <p>
        *Вывести с помощью цикла for числа от 0 до 9, НЕ используя тело цикла.
    </p>
    <?php
        global $a;
        $a = 0;
            for(;$a <= 9; iter()){
                //Empty
            }
        function iter() 
        {
            global $a;
            echo $a . ' ';
            $a++;
        }   
    ?>
    <h1>Задание 8</h1>
    <p>
        *Повторить третье задание, но вывести на экран только города, начинающиеся с буквы «К»
    </p>
    <?php
        foreach($cities as $key => $city) {
            echo "<p>" . $key . "</p>";
            foreach($city as $key => $value) {
                if (mb_substr($value, 0, 1) == "К") echo $value . ", ";
            }
        }
    ?>
    <h1>Задание 8</h1>
    <p>
        *Объединить две ранее написанные функции в одну, которая получает строку на русском языке, производит транслитерацию и замену пробелов на подчеркивания
        <br>Решение: добавил в массив алфавита ключ пробел и значение нижнее подчеркивание
    </p>
</body>
</html>
