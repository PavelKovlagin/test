<html>
<head>
    <title>Upload file</title>
    <meta charset="utf-8">
</head>
<body>
    <?php
        ini_set("error_reporting", E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        echo "<pre>";
        print_r($_FILES);
        echo "</pre>";
        
        var_dump(__DIR__);
        $uploads_dir = __DIR__.'/uploads';
        if ($_FILES["file"]["error"] == 0) {
            $tmp_name = $_FILES["file"]["tmp_name"];
            // basename() может предотвратить атаку на файловую систему;
            // может быть целесообразным дополнительно проверить имя файла
            $name = basename($_FILES["file"]["name"]);
            var_dump(move_uploaded_file($tmp_name, "$uploads_dir/$name"));
        }
    ?>
</body>
</html>