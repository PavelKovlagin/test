<html>
<head>
    <title>Lesson 1</title>
    <meta charset="utf-8">
</head>
<body>
    <?php 
        if($_POST && ($variable_a = $_POST['variable_a']) && ($variable_b = $_POST['variable_b'])) {
            echo 'Оригинал: $a='. $variable_a . '; $b=' . $variable_b . '<br>';
            $variable_a = $variable_a + $variable_b;
            $variable_b = $variable_a - $variable_b;
            $variable_a = $variable_a - $variable_b;
            echo 'После изменения: $a=' . $variable_a . '; $b=' . $variable_b;
        } else {
            echo "<h1>Переменные не найдены</h1>";
        }
        
    ?>

    <form method="POST">
        <p>variable a <input name="variable_a" type="number"></p>
        <p>variable b <input name="variable_b" type="number"></p>
        <input name="button" type="submit" value="Отправить">
    </form>    
</body>
</html>