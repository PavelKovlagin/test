<html>
<head>
    <title>Lesson 2</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Задание 1</h1>
    <p>
        Объявить две целочисленные переменные $a и $b и задать им произвольные начальные значения. Затем написать скрипт, который работает по следующему принципу:
        <br> a. Если $a и $b положительные, вывести их разность.
        <br> b. Если $а и $b отрицательные, вывести их произведение.
        <br> c. Если $а и $b разных знаков, вывести их сумму.
        <br> Ноль можно считать положительным числом.
    </p>
    <?php 
        if ($_POST && ($variable_a = $_POST['task1_variable_a']) && ($variable_b = $_POST['task1_variable_b'])) {
            if ($variable_a >= 0 && $variable_b >= 0) {
                echo $variable_a . '-' . $variable_b . '=' . ($variable_a-$variable_b);
            } elseif($variable_a < 0 && $variable_b < 0) {
                echo $variable_a . '*' . $variable_b . '=' . ($variable_a*$variable_b);
            } else {
                echo $variable_a . '+' . $variable_b . '=' . ($variable_a+$variable_b);
            }
        } else {
            echo "Переменные пустые";
        }
    ?>
    <form method="POST">
        <input name="task1_variable_a" type="number">
        <input name="task1_variable_b" type="number">
        <input name="button" type="submit" value="Отправть">
    </form>
    <h1>Задание 2</h1>
    <p>
        Присвоить переменной $а значение в промежутке [0..15]. С помощью оператора switch организовать вывод чисел от $a до 15.
    </p>
    <?php
        if ($_POST && ($variable_a = $_POST['task2_variable_a'])) {
            switch ($variable_a) {
                case 1:
                    echo '1 ';
                case 2:
                    echo '2 ';
                case 3:
                    echo '3 ';
                case 4:
                    echo '4 ';
                case 5:
                    echo '5 ';
                case 6:
                    echo '6 ';
                case 7:
                    echo '7 ';
                case 8:
                    echo '8 ';
                case 9:
                    echo '9 ';
                case 10:
                    echo '10 ';
                case 11:
                    echo '11 ';
                case 12:
                    echo '12 ';
                case 13:
                    echo '13 ';
                case 14:
                    echo '14 ';
                case 15:
                    echo '15 ';
                break;
                default:
                    echo "Число больше 15";
            }
        } else {
            echo "Переменная пуста";
        }
    ?>
    <form method="POST">
        <input name="task2_variable_a" type="number">
        <input type="submit" name="button" value="Отправить">    
    </form>
    <h1>Задание 3</h1>
    <p>
    Реализовать основные 4 арифметические операции в виде функций с двумя параметрами. Обязательно использовать оператор return
    </p>
    <form method="POST">
        <input name="task3_variable_a" type="number">
        <input name="task3_variable_b" type="number">
        <button>Отправить</button>
    </form>
    <?php
        function summation(int $a, int $b) {
            return $a + $b;
        }

        function subtraction(int $a, int $b) {
            return $a - $b;
        }

        function multiply(int $a, int $b) {
            return $a * $b;
        }

        function division(int $a, int $b) {
            return $a / $b;
        }
        if ($_POST && ($variable_a = $_POST['task3_variable_a']) && ($variable_b = $_POST['task3_variable_b'])) {
            echo $variable_a . '+' . $variable_b . '=' . summation($variable_a, $variable_b);
            echo '<br>' . $variable_a . '-' . $variable_b . '=' . subtraction($variable_a, $variable_b);
            echo '<br>' . $variable_a . '*' . $variable_b . '=' . multiply($variable_a, $variable_b);
            echo '<br>' . $variable_a . '/' . $variable_b . '=' . division($variable_a, $variable_b);
        } else {
            echo "Переменные пустые";
        }
    ?>
    <h1>Задание 4</h1>
    <p>
        Реализовать функцию с тремя параметрами: function mathOperation($arg1, $arg2, $operation), где $arg1, $arg2 – значения аргументов, $operation – строка с названием операции. В        зависимости от переданного значения операции выполнить одну из арифметических операций (использовать функции из пункта 3) и вернуть полученное значение (использовать switch).
    </p>
    <form method="POST">
        <input name="task4_variable_a" type="number">
        <select name="sign">
            <option value="+">+</option>
            <option value="-">-</option>
            <option value="*">*</option>
            <option value="/">/</option>
        </select>
        <input name="task4_variable_b" type="number">
        <button>Отправить</button>
    </form>
    <?php 
        function mathOperation(int $arg1, int $arg2, $operation) {
            switch ($operation) {
                case '+':
                    echo summation($arg1, $arg2);
                    break;  
                case '*':
                    echo multiply($arg1, $arg2);
                break;  
                case "-":
                    echo subtraction($arg1, $arg2);
                break;
                case "/":
                    echo division($arg1, $arg2);
                break;
                default:
                    echo "nothing";
                    break;
            }
        } 
        if ($_POST 
        && ($variable_a = $_POST['task4_variable_a']) 
        && ($variable_b = $_POST['task4_variable_b']) 
        && ($sign = $_POST['sign'])) {
            mathOperation($variable_a, $variable_b, $sign);
        } else {
            echo "POST пуст";
        }        
    ?>
    <h1>Задание 6</h1>
    <p>
    *С помощью рекурсии организовать функцию возведения числа в степень. Формат: function power($val, $pow), где $val – заданное число, $pow – степень
    </p>
    <form method="POST">
        <input name="task6_variable_a" type="number">
        <input name="task6_power" type="number">
        <button>Отправить</button> 
    </form>
    <?php
        function power($var, $pow) {
            if ($pow == 0) return 1;
            if ($pow == 1) return $var;
            return power($var, $pow-1) * $var;
        } 
        if ($_POST
            && ($variable_a = $_POST['task6_variable_a'])
            && ($pow = $_POST['task6_power'])) {
                echo $variable_a . '^' . $pow . '=' . power($variable_a, $pow);
        } else {
            echo "POST пуст";
        }
    ?>
    <h1>Задание 7</h1>
    <p>
    *Написать функцию, которая вычисляет текущее время и возвращает его в формате с правильными склонениями, например: 22 часа 15 минут, 21 час 43 минуты.
    </p>
    <form method="POST">
        <p>Часы: <input name="task7_hour" type="number"></p>
        <p>Минуты: <input name="task7_minute" type="number"></p>
        <p>Секунды: <input name="task7_second" type="number"></p>
        <button>Отправить</button>
    </form>
    <?php
        // function hourToString(int $hour) {
        //     $hour = abs($hour);
        //     if ($hour >= 11 && $hour <= 19) return $hour . " часов";
        //     $last = $hour%10;
        //     if ($last == 0) return $hour . " часов";
        //     if ($last == 1) return $hour . " час";
        //     if ($last >= 2  && $last <= 4) return $hour . " часа";
        //     if ($last >= 5 && $last <= 9) return $hour . " часов";
        // }

        // function minutesToString(int $minute) {
        //     $minute = abs($minute);
        //     if ($minute >= 11 && $minute <= 19) return $minute . " минут";
        //     $last = $minute%10;
        //     if ($last == 0) return $minute . " минут";
        //     if ($last == 1) return $minute . " минута";
        //     if ($last >= 2  && $last <= 4) return $minute . " минуты";
        //     if ($last >= 5 && $last <= 9) return $minute . " минут";
        // }

        // function secondsToString(int $second) {
        //     $second = abs($second);
        //     if ($second >= 11 && $second <= 19) return $second . " секунд";
        //     $last = $second%10;
        //     if ($last == 0) return $second . " секунд";
        //     if ($last == 1) return $second . " секунда";
        //     if ($last >= 2  && $last <= 4) return $second . " секунды";
        //     if ($last >= 5 && $last <= 9) return $second . " секунд";
        // }

        //долго сидел и думал как бы назвать функцию и параметры, и так ничего и не придумал
        //$ip - именительный падеж
        //$rp - родительный падеж
        //$vp - винительный падеж
        function cases(int $number, $ip, $rp, $vp)
        {
            $number = abs($number);
            if ($number >= 11 && $number <= 19) return $number . " " . $rp;
            $last = $number%10;
            if ($last == 0) return $number . " " . $rp;
            if ($last == 1) return $number . " " . $ip;
            if ($last >= 2  && $last <= 4) return $number . " " . $vp;
            if ($last >= 5 && $last <= 9) return $number . " " . $rp;
        }

        if ($_POST
            && ($hour = $_POST['task7_hour'])
            && ($minute = $_POST['task7_minute'])
            && ($second = $_POST['task7_second'])
            ) {
            echo cases($hour, "час", "часов", "часа") . " " . cases($minute, "минута", "минут", "минуты") . " " .  cases($second, "секунда", "секунд", "секунды") . " ";
        } else {
            echo "Time not exist ";
        }   
        
    ?>

        