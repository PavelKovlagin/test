<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <a href="gallery1.php">Галерея 1</a>
    <a href="gallery2.php">Галерея 2</a>    

    <form method="POST" enctype="multipart/form-data">
        <p>Название файла: <input name="filename" type="text" value=""></p>
        <p>Файл: <input name="file" type="file" value=""></p>
        <input type="submit" value="Отправить">
    </form>

    <?php
        ini_set("error_reporting", E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);

        function openCsv() {
            $file = fopen('images.csv', 'r');
            if ($file !== false) {
                echo '<table>';
                echo '<tr><td>Оригинальное имя файла</td><td>Имя файла</td><td>Дата загрузки</td></tr>';
                while (($data = fgetcsv($file, 1000, '|')) !== false) {                    
                    echo '<tr>';
                    foreach ($data as $key => $value) {
                        echo '<td>' . $data[$key] . '</td>';
                    }
                    echo '</tr>';
                }
                echo '</table>';
                fclose($file);
            }
        }
        
        $uploads_dir = __DIR__.'/images';
        if ($_POST && ($_POST['filename']) && ($_FILES["file"]["error"] == 0)) {
            $tmp_name = $_FILES["file"]["tmp_name"];
            $filename = $_POST['filename'];
            $name = basename($_FILES["file"]["name"]);
            $type = preg_split('/\//', $_FILES['file']['type'])[1];
            $imageData = [
                $name,
                $filename . '.' . $type,
                date('d.m.Y H:m:s')
            ];
            $file = fopen('images.csv', 'a+');         
            if ($file !== false) {
                move_uploaded_file($tmp_name, "$uploads_dir/$filename.$type");
                fputcsv($file, $imageData, '|');
                fclose($file);
                
            } else {
                echo '<h2>Error file open</h2>';
            }   
        } else {
            echo "<h2>POST пуст</h2>";
        }
        openCsv();
    ?>
</body>
</html>