<html>
<head>
    <title>Gallery 2</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <script>
    function showModal(img) {
        var modal = document.getElementById('myModal').style.display = "block";
        var modalImg = document.getElementById('img01').src = img.src;
        var captionText = document.getElementById('caption').innerHTML = img.alt;   
    }

    function closeModal() {
        document.getElementById('myModal').style.display = 'none';
    }
    </script>

    <form method="POST">
        <?php
            $files = scandir(__DIR__);
            unset($files[0],$files[1]); // удаление . и ..
            foreach ($files as $file) {
                if (is_dir($file)) {
                    echo '<p><input name="imageDir" type="radio" value="' . $file . '">' . $file . "</p>";
                }
            }
        ?>
        <button>Открыть директорию</button>

        <?php
            function showImage($dir) {
                $files = scandir(__DIR__ . '/' . $dir);
                foreach ($files as $file) {
                    if (preg_match('/(.jpg|.png|.jpeg)$/', $file)) {
                        echo '<p><img width="200px" alt="'.$file.'" src=" ' . $dir . '/' . $file . '" onclick=showModal(this)></p>';
                    }
                }
            }
            if ($_POST && $_POST['imageDir']) {
                showImage($_POST['imageDir']);
            } else {
                echo "<h2>Please select catalog</h2>";
            }
        ?>
    </form>

    <div id="myModal" class="modal">
        <span class="close" onclick=closeModal()>&times;</span>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
    </div>

</body>
</html>