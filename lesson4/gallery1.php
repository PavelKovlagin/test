<html>
<head>
    <meta charset="utf-8">
</head>
<body>
    <?php
        ini_set("error_reporting", E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);

        $dir = 'images/';
        $files = scandir(__DIR__ . '/' . $dir);
        foreach ($files as $file) {
            if (preg_match('/(.jpg|.png|.jpeg)/', $file)) {
                echo '<p>' . $dir .$file . '</p>';
                echo '<img width="200px" src=' . $dir .  $file . ' onclick="window.open(this.src)">';
            }
        }
    ?>
</body>
</html>